# Dockerfiles

This repo contains the following:

- Anaconda 4.4.0
- PyTorch master
- Tensorflow latest from pip (tensorflow-gpu)


**Note**: The repository is quite large (9 GB), however it should have everything necessary to run nearly any deep learning project we need. Pull requests for shrinking the image size or other optimizations are welcome.

### Build Instructions

#### Python 2.7
To build:
`nvidia-docker build -f Dockerfile.py27.gpu -t $USER/anaconda2-pytorch-tensorflow:latest .`

To run:
`nvidia-docker run -it $USER/anaconda2-pytorch-tensorflow`

#### Python 3.6
To build:
`nvidia-docker build -f Dockerfile.py36.gpu -t $USER/anaconda3-pytorch-tensorflow:latest .`

To run:
`nvidia-docker run -it $USER/anaconda3-pytorch-tensorflow`

